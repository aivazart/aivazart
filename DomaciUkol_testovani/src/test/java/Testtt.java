import archive.ItemPurchaseArchiveEntry;
import archive.PurchasesArchive;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mockito;
import org.mockito.internal.matchers.Or;
import shop.*;
import org.junit.jupiter.api.Test;
import storage.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

public class Testtt {
    @Test
    public void standardItem_Constructor_Test() {
        int id = 1;
        String name = "name";
        float price = 10000.999f;
        String category = "category";
        int loyaltyPoints = 10;
        StandardItem item = new StandardItem(id, name, price, category, loyaltyPoints);
        assertEquals(id, item.getID());
        assertEquals(name, item.getName());
        assertEquals(price, item.getPrice());
        assertEquals(category, item.getCategory());
        assertEquals(loyaltyPoints, item.getLoyaltyPoints());
    }

    @Test
    public void copy_Test() {
        StandardItem item1 = new StandardItem(1, "name", 222.22f, "categoty", 10);
        StandardItem item2 = item1.copy();
        assertEquals(item1, item2);
    }

    @ParameterizedTest
    @CsvSource({"1, name, 10.1f, category, 5, 1, name, 10.1f, category, 5, true",
            "1, name, 10.1f, category, 5, 2, name, 10.1f, category, 5, false",
            "333, nameeeeeeee, 10.1f, category1, 5, 2, nameee, 10.1f, category2, 5, false",
            "3, Name, 10.1111f, category, 5, 3, Name, 10.1111f, category, 5, true"})
    public void equals_Param_Test(int id1, String name1, float price1, String category1, int loyaltyPoints1,
                                  int id2, String name2, float price2, String category2, int loyaltyPoints2, boolean isEqual) {
        StandardItem item1 = new StandardItem(id1, name1, price1, category1, loyaltyPoints1);
        StandardItem item2 = new StandardItem(id2, name2, price2, category2, loyaltyPoints2);
        assertEquals(item1.equals(item2), isEqual);
    }

    @Test
    public void order_ConstructorWithStateTest() {
        ArrayList<Item> list= new ArrayList<Item>();
        list.add(new StandardItem(1, "name", 222.22f, "categoty", 10));
        list.add(new StandardItem(2, "name", 222.22f, "categoty", 10));
        list.add(new StandardItem(3, "name", 222.22f, "categoty", 10));
        list.add(new StandardItem(4, "name", 222.22f, "categoty", 10));
        ShoppingCart cart = new ShoppingCart(list);
        String customerName = "John";
        String customerAddress = "Nekrasova7";
        int state = 10;
        Order order = new Order(cart, customerName, customerAddress, state);
        assertNotNull(order);
        assertEquals(cart.getCartItems(), order.getItems());
        assertEquals(customerName, order.getCustomerName());
        assertEquals(customerAddress, order.getCustomerAddress());
        assertEquals(state, order.getState());
    }
    @Test
    public void order_ConstructorWithoutStateTest() {
        ArrayList<Item> list = new ArrayList<Item>();
        list.add(new StandardItem(1, "name", 222.22f, "categoty", 10));
        list.add(new StandardItem(2, "name", 222.22f, "categoty", 10));
        list.add(new StandardItem(3, "name", 222.22f, "categoty", 10));
        list.add(new StandardItem(4, "name", 222.22f, "categoty", 10));
        ShoppingCart cart = new ShoppingCart(list);
        String customerName = "Alex";
        String customerAddress = "U_Svobodarny";
        Order order = new Order(cart, customerName, customerAddress);
        assertNotNull(order);
        assertEquals(cart.getCartItems(), order.getItems());
        assertEquals(customerName, order.getCustomerName());
        assertEquals(customerAddress, order.getCustomerAddress());
        assertEquals(0, order.getState());
    }

    private class ItemForTestClass extends Item {
        public ItemForTestClass(int id, String name, float price, String category) {
            super(id, name, price, category);
        }
    }
    @Test
    public void itemStock_ConstructorTest() {
        Item item = new ItemForTestClass(1, "Test Item", 10.0f, "Test Category");
        ItemStock itemStock = new ItemStock(item);
        assertEquals(item, itemStock.getItem());
        assertEquals(0, itemStock.getCount());
    }

    @ParameterizedTest
    @CsvSource({"3, 10, 13, true", "2, 3, 5, true", "3, 3, 6, true", "3, 0, 0, false"})
    public void itemStock_testIncreaseItemCount(int increaseCount1, int increaseCount2, int expectedCount, boolean isCorrect) {
        Item item = new ItemForTestClass(1, "testItem", 10.0f, "testCategory");
        ItemStock itemStock = new ItemStock(item);
        itemStock.increaseItemCount(increaseCount1);
        itemStock.increaseItemCount(increaseCount2);
        boolean isOk = itemStock.getCount() == expectedCount;
        assertEquals(isOk, isCorrect);
    }

    @ParameterizedTest
    @CsvSource({"3, 10, 0, true", "3, 10, -7, false", "3, 3, 0, true", "3, 0, 3, true"})
    public void itemStock_testDecreaseItemCount(int increaseCount, int decreaseCount, int expectedCount, boolean isCorrect) {
        Item item = new ItemForTestClass(1, "testItem", 10.0f, "testCategory");
        ItemStock itemStock = new ItemStock(item);
        itemStock.increaseItemCount(increaseCount);
        itemStock.decreaseItemCount(decreaseCount);
        boolean isOk = itemStock.getCount() == expectedCount;
        assertEquals(isOk, isCorrect);
    }

    @Test
    public void purchasesArchive_printItemPurchaseStatistics_test(){
        ItemPurchaseArchiveEntry entry = mock(ItemPurchaseArchiveEntry.class);
        when(entry.toString()).thenReturn("ITEM  Item ID id   NAME name   CATEGORY category   HAS BEEN SOLD 1 TIMES");

        HashMap<Integer, ItemPurchaseArchiveEntry> itemArchive = new HashMap<>();
        itemArchive.put(1, entry);

        ArrayList<Order> orderArrayList = new ArrayList<>();

        PurchasesArchive purchasesArchive = new PurchasesArchive(itemArchive, orderArrayList);

        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        purchasesArchive.printItemPurchaseStatistics();

        String expected = "ITEM PURCHASE STATISTICS:\n" +
                "ITEM  Item ID id   NAME name   CATEGORY category   HAS BEEN SOLD 1 TIMES\n";
        assertEquals(expected, outContent.toString());
    }

    @Test
    public void purchasesArchive_getHowManyTimesHasBeenItemSold_test() {


//        //Creating items
//        Item item = new StandardItem(1, "ItemName1", 10.0f, "Category1", 20);
//
//        //Creating entries
//        ItemPurchaseArchiveEntry itemEntry = new ItemPurchaseArchiveEntry(item);
//
//        //Creating itemArchive
//        HashMap<Integer, ItemPurchaseArchiveEntry> itemArchive = new HashMap<>();
//        itemArchive.put(1, itemEntry);
//
//        // Creating orderArchive
//        ArrayList<Order> orderArchive = new ArrayList<>();
//
//        //Creating purchasesArchive
//        PurchasesArchive purchasesArchive = new PurchasesArchive(itemArchive, orderArchive);
//        assertEquals(1, purchasesArchive.getHowManyTimesHasBeenItemSold(item));

//        Item item1 = new ItemForTestClass(1, "name1", 10.0f, "category1");
//
//        ItemPurchaseArchiveEntry entry1 = new ItemPurchaseArchiveEntry(item1);
        ItemPurchaseArchiveEntry entry = mock(ItemPurchaseArchiveEntry.class);
        Mockito.when(entry.getCountHowManyTimesHasBeenSold()).thenReturn(1);

//        HashMap<Integer, ItemPurchaseArchiveEntry> integerItemPurchaseArchiveEntryHashMap = mock(HashMap.class);
//        integerItemPurchaseArchiveEntryHashMap.put(1, entry1);
//        when(integerItemPurchaseArchiveEntryHashMap.containsKey(1)).thenReturn(true);

        Mockito.verify(entry).getCountHowManyTimesHasBeenSold();
    }

    @Test
    public void purchasesArchive_putOrderToPurchasesArchive_test(){
        Item item1 = mock(ItemForTestClass.class);
        Item item2 = mock(ItemForTestClass.class);

        when(item1.getID()).thenReturn(1);
        when(item1.getName()).thenReturn("Box");
        when(item1.getPrice()).thenReturn(10.0f);
        when(item1.getCategory()).thenReturn("Things");

        when(item2.getID()).thenReturn(2);
        when(item2.getName()).thenReturn("Cup");
        when(item2.getPrice()).thenReturn(5.0f);
        when(item2.getCategory()).thenReturn("Dishes");

        ShoppingCart cart1 = mock(ShoppingCart.class);
        cart1.addItem(item1);
        Order order1 = new Order(cart1, "Martin Novak", "U svobodarny 11");


        ShoppingCart cart2 = mock(ShoppingCart.class);
        cart2.addItem(item2);
        Order order2 = new Order(cart2, "Jan Novak", "Panska 6");

        PurchasesArchive purchasesArchive = mock(PurchasesArchive.class);
        purchasesArchive.putOrderToPurchasesArchive(order1);
        purchasesArchive.putOrderToPurchasesArchive(order2);

        ArrayList<Order> orders = purchasesArchive.getOrderArchive();

        assertEquals(2, orders.size());
    }

    @Test public void purchasesArchive_orderArchiveTest() {
        PurchasesArchive purchasesArchive = mock(PurchasesArchive.class);
        when(purchasesArchive.getOrderArchive()).thenReturn(new ArrayList<>());
        assertEquals(new ArrayList<Order>(), purchasesArchive.getOrderArchive());
    }


    
}



