package hw2;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

public class CalculatorTest {

    private Calculator cal;
    @Test
    public void sum_returnsSum_return4(){
        cal = new Calculator();
        int sum = cal.add(2, 2);
        int expectedRes = 4;
        Assertions.assertEquals(expectedRes, sum);
    }

    @Test
    public void subtract_returnsSubtract_return4(){
        cal = new Calculator();
        int subtract = cal.subtract(6, 2);
        int expectedRes = 4;
        Assertions.assertEquals(expectedRes, subtract);
    }

    @Test
    public void multiply_returnsMultiply_return10() {
        cal = new Calculator();
        int multiply = cal.multiply(5, 2);
        int expectedRes = 10;
        Assertions.assertEquals(expectedRes, multiply);
    }
    @Test
    @Order(1)
    public void divide_returnsDivide_return10() {
        cal = new Calculator();
        int divide = cal.divide(100, 10);
        int expectedRes = 10;
        Assertions.assertEquals(expectedRes, divide);
    }

    @Test
    @Order(2)
    public void exceptionThrownTest_exceptionThrown_exception() {
        //ARRANGE
        cal = new Calculator();

        // ACT + ASSERT 1 (Obsahuje assert, zda byla vyhozena výjimka očekávaného typu)
        Exception exception = Assertions.assertThrows(Exception.class, () -> cal.divide(2, 0));

//        Exception exception = Assertions.assertThrows(Exception.class, new Executable() {
//            @Override
//            public void execute() throws Throwable {
//                foo.exceptionThrown();
//            }
//        });


        String expectedMessage = "/ by zero";
        String actualMessage = exception.getMessage();

        // ASSERT 2
        Assertions.assertEquals(expectedMessage, actualMessage);
    }

}
